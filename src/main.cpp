#include <Arduino.h>
#include <IRremote.h>
#include <LowPower.h>


#define ENCODER_DO_NOT_USE_INTERRUPTS
#include <Encoder.h>

// #define myDEBUG
#define TIME_BETWEEN_SENDS       100
#define MIN_TIME_FOR_3_SENDS    1100
#define WAKETIMEINMILLIS        2000    // 2 seconds

#define rotarySwitchPin            4    // choose the input pin (for a pushbutton)
#define wakeUpPin                  2    // Use pin 2 as wake up pin

enum SonyOutputFormatState
{
    Slow = 0,
    Fast = 1
};


IRsend                  irsend;
long                    oldPosition             = 0;
unsigned long           lastTimeSend            = millis();
unsigned long           previousMillis          = millis();

unsigned long           volumeUpTimings[3]      = {0, 0, 0};
unsigned long           volumeDownTimings[3]    = {0, 0, 0};

SonyOutputFormatState   currentState            = Slow;
int                     lastSwitchValue         = HIGH;


// Change these two numbers to the pins connected to your encoder.
//   Best Performance: both pins have interrupt capability
//   Good Performance: only the first pin has interrupt capability
//   Low Performance:  neither pin has interrupt capability
//   avoid using pins with LEDs attached
Encoder myEnc(5, 6);


// ###########################################


static bool goToSleep()
{
    if (((unsigned long)(millis() - previousMillis)) >= WAKETIMEINMILLIS)
    {
        return true;
    }
    else
    {
        return false;
    } /* end if */
} /* end goToSleep */


static void wakeUp()
{
    previousMillis = millis();

    #ifdef myDEBUG
    Serial.println("Woke up");
    #endif
} /* end wakeUp */


static void powerDown()
{
    attachInterrupt(0, wakeUp, LOW); // Allow wakeUp pin to trigger interrupt on low.

    #ifdef myDEBUG
    Serial.println("PowerDown");
    #endif

    LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF);

    detachInterrupt(0);
} /* end powerDown */


static void lowPowerSleep(period_t period)
{
    LowPower.idle(period, ADC_OFF, TIMER2_OFF, TIMER1_OFF, TIMER0_ON,
                  SPI_OFF, USART0_OFF, TWI_OFF);
} /* end lowPowerSleep */


// ###########################################


static boolean pushTiming(unsigned long *queue, unsigned long timing)
{
    if (timing - queue[0] < MIN_TIME_FOR_3_SENDS)
    {
        return false;
    }
    
    queue[0] = queue[1];
    queue[1] = queue[2];
    queue[2] = timing;
    return true;
} /* end pushTiming */


static void updateOutputStreamFormat()
{
    int switchValue = 0;

    switchValue = digitalRead(rotarySwitchPin); // read input value
    if (switchValue != lastSwitchValue)
    {
        if (switchValue == HIGH)
        { 
            // check if the input is HIGH (button released)
            #ifdef myDEBUG
            Serial.println("depressed");
            #endif
            lastSwitchValue = HIGH;
        }
        else
        {
            if (currentState == Slow)
            {
                #ifdef myDEBUG
                Serial.println("FAST mode");
                #endif
                currentState = Fast;
            }
            else
            {
                #ifdef myDEBUG
                Serial.println("SLOW mode");
                #endif
                currentState = Slow;
            } /* end if */

            #ifdef myDEBUG
            Serial.println("pressed");
            #endif
            lastSwitchValue = LOW;
        } /* end if */
        lowPowerSleep(SLEEP_120MS);
        previousMillis = millis();
    } /* end if */
} /* end updateOutputStreamFormat */


static void volumeUp()
{
    #ifdef myDEBUG
    Serial.println("volumeUp");
    #endif

    if ((currentState == Slow) && (pushTiming(volumeUpTimings, millis()) == false))
    {
        return;
    }
    
    irsend.sendSony(0x486, 12);
    lowPowerSleep(SLEEP_15MS);
    irsend.sendSony(0x486, 12);
    lowPowerSleep(SLEEP_15MS);
    irsend.sendSony(0x486, 12);
    lowPowerSleep(SLEEP_15MS);
    irsend.sendSony(0x486, 12);
    lowPowerSleep(SLEEP_15MS);
    irsend.sendSony(0x486, 12);
} /* end volumeUp */


static void volumeDown()
{
    #ifdef myDEBUG
    Serial.println("volumeDown");
    #endif

    if ((currentState == Slow) && (pushTiming(volumeDownTimings, millis()) == false))
    {
        return;
    }

    irsend.sendSony(0xC86, 12);
    lowPowerSleep(SLEEP_15MS);
    irsend.sendSony(0xC86, 12);
    lowPowerSleep(SLEEP_15MS);
    irsend.sendSony(0xC86, 12);
    lowPowerSleep(SLEEP_15MS);
    irsend.sendSony(0xC86, 12);
    lowPowerSleep(SLEEP_15MS);
    irsend.sendSony(0xC86, 12);
} /* end volumeDown */


// ###########################################


void setup()
{
    #ifdef myDEBUG
    Serial.begin(115200);
    Serial.println("Setup finished");
    pinMode(LED_BUILTIN, OUTPUT); // declare LED as output
    #endif
    pinMode(rotarySwitchPin, INPUT);    // declare pushbutton as input
    previousMillis = millis();
} /* end setup */


void loop()
{
    long newPosition = myEnc.read();

    if (newPosition != oldPosition)
    {
        if (((unsigned long)(millis() - lastTimeSend)) > TIME_BETWEEN_SENDS)
        {
            #ifdef myDEBUG
            Serial.println(newPosition);
            #endif
            if (newPosition > oldPosition)
            {
                volumeDown();
            }
            else if (newPosition < oldPosition)
            {
                volumeUp();
            }
            lastTimeSend = millis();
            previousMillis = millis();
        }
        oldPosition = newPosition;
        #ifdef myDEBUG
        Serial.println(newPosition);
        #endif
    }

    updateOutputStreamFormat();

    if (goToSleep() == true)
        powerDown();
} /* end loop */
